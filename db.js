const {MongoClient} = require('mongodb');

let dbConnection

module.exports = {
    connectDB: callback => {
        MongoClient.connect('mongodb://localhost:27017/config')
            .then(client => {
                console.log("Connected to database")
                dbConnection = client.db()
                return callback()
            })
            .catch(err => {
                console.error(err)
                return callback(err)
            })
    },
    getDB: () => dbConnection
}