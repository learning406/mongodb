const express = require("express")
const { ObjectId } = require("mongodb")
const { connectDB, getDB } = require("./db")

// init app and middleware
const app = express()

// db connection
let db

connectDB(err => {
    if (!err) {
        app.listen(3000, () => {
            console.log("Server started on port 3000")
        })
    }

    db = getDB()
})

// routes
app.get("/books", (req, res) => {
    // current page
    const page = parseInt(req.query.page) || 1
    // items per page
    const limit = parseInt(req.query.limit) || 10

    let books = []

    // db.books.find()
    db.collection("books")
        .find()
        .sort({ author: 1 })
        .skip(page * limit)
        .limit(limit)
        .forEach(book => books.push(book))
        .then(() => {
            res.status(200).json(books)
        })
        .catch(err => {
            console.error(err)
            res.status(500).json({ error: "could not fetch data" })
        })
    
    //res.json({mssg: "welcome to the api"})
})

app.get('/books/:id', (req, res) => {

    if (ObjectId.isValid(req.params.id)) {
        db.collection("books")
            .findOne({ _id: ObjectId(req.params.id) })
            .then(doc => {
                res.status(200).json(doc)
            })
            .catch(err => {
                console.error(err)
                res.status(500).json({ error: "could not fetch the document" })
            })
    } else {
        res.status(500).json({ error: "Not a valid doc id" })
    }
})

app.post('/books', (req, res) => {
    const book = req.body

    db.collection("books")
        .insertOne(book)
        .then(result => {
            res.status(200).json(result)
        })
        .catch(err => {
            console.error(err)
            res.status(500).json({ error: "could not insert the document" })
        })
})

app.delete('/books/:id', (req, res) => {

    if (ObjectId.isValid(req.params.id)) {
        db.collection("books")
            .deleteOne({ _id: ObjectId(req.params.id) })
            .then(result => {
                res.status(200).json(result)
            })
            .catch(err => {
                console.error(err)
                res.status(500).json({ error: "could not delete the document" })
            })
    } else {
        res.status(500).json({ error: "Not a valid doc id" })
    }
})

app.patch('/books/:id', (req, res) => {
    const update = req.body

    if (ObjectId.isValid(req.params.id)) {
        db.collection("books")
            .updateOne({ _id: ObjectId(req.params.id) }, { $set: update })
            .then(result => {
                res.status(200).json(result)
            })
            .catch(err => {
                console.error(err)
                res.status(500).json({ error: "could not update the document" })
            })
    } else {
        res.status(500).json({ error: "Not a valid doc id" })
    }
})